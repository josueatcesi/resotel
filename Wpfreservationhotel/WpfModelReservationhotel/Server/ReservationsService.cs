﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfModelReservationhotel.Server
{
    class ReservationsService
    {

        #region Singleton

        private MySqlConnection connection = null;

        private bool OpenConnection()
        {
            try
            {
                MySqlConnectionStringBuilder connectionStringBuilder = new MySqlConnectionStringBuilder();
                connectionStringBuilder.Server = "localhost:8888";
                connectionStringBuilder.Database = "gestioncontacts";
                connectionStringBuilder.UserID = "root";
                connectionStringBuilder.Password = "root";
                //String stringDeConnexion = connectionStringBuilder.ConnectionString
                String stringDeconnexion = "(localdb)\\MSSQLLocalDB; Initial Catalog = master; Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = False; ApplicationIntent = ReadWrite; MultiSubnetFailover = False";
                connection = new MySqlConnection(stringDeconnexion);

                connection.Open();

                return true;
            }
            catch
            {
                connection = null;
                return false;
            }

        }

        private static ReservationsService _instance = null;
        public static ReservationsService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ReservationsService();
                }
                return _instance;

            }
        }

        //empeche instanciation de la classe
        private ReservationsService()
        {

        }

        #endregion

    }
}
