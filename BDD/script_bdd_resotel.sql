/*------------------------------------------------------------
*        Script SQLSERVER 
------------------------------------------------------------*/


/*------------------------------------------------------------
-- Table: Client
------------------------------------------------------------*/
CREATE TABLE Client(
	id        INT IDENTITY (1,1) NOT NULL ,
	nom       CHAR (20)  NOT NULL ,
	prenom    CHAR (20)  NOT NULL ,
	email     VARCHAR (100) NOT NULL ,
	tel       VARCHAR (10) NOT NULL ,
	adresse   VARCHAR (100) NOT NULL  ,
	CONSTRAINT Client_PK PRIMARY KEY (id)
);


/*------------------------------------------------------------
-- Table: OptionService
------------------------------------------------------------*/
CREATE TABLE OptionService(
	id           INT IDENTITY (1,1) NOT NULL ,
	libelle      VARCHAR (20) NOT NULL ,
	prix         FLOAT   ,
	nbPersonne   INT  NOT NULL ,
	prixPromo    FLOAT    ,
	CONSTRAINT OptionService_PK PRIMARY KEY (id)
);


/*------------------------------------------------------------
-- Table: RoleUtilisateur
------------------------------------------------------------*/
CREATE TABLE RoleUtilisateur(
	id        INT IDENTITY (1,1) NOT NULL ,
	libelle   CHAR (5)  NOT NULL  ,
	CONSTRAINT RoleUtilisateur_PK PRIMARY KEY (id)
);


/*------------------------------------------------------------
-- Table: Employe
------------------------------------------------------------*/
CREATE TABLE Employe(
	id                   INT IDENTITY (1,1) NOT NULL ,
	nom                  CHAR (20)  NOT NULL ,
	prenom               CHAR (20)  NOT NULL ,
	login                VARCHAR (20) NOT NULL ,
	mdp                  VARCHAR (20) NOT NULL ,
	email                VARCHAR (100) NOT NULL ,
	id_RoleUtilisateur   INT  NOT NULL  ,
	CONSTRAINT Employe_PK PRIMARY KEY (id)

	,CONSTRAINT Employe_RoleUtilisateur_FK FOREIGN KEY (id_RoleUtilisateur) REFERENCES RoleUtilisateur(id)
);


/*------------------------------------------------------------
-- Table: Chambre
------------------------------------------------------------*/
CREATE TABLE Chambre(
	numero            INT  NOT NULL ,
	capacite          INT  NOT NULL ,
	prixUneNuit       FLOAT  NOT NULL ,
	prixCinqNuit      FLOAT  NOT NULL ,
	prixDixNuit       FLOAT  NOT NULL ,
	aNettoyer         bit  NOT NULL ,
	litBebe           bit  NOT NULL ,
	balcon            bit  NOT NULL ,
	coursInterieure   bit  NOT NULL  ,
	CONSTRAINT Chambre_PK PRIMARY KEY (numero)
);


/*------------------------------------------------------------
-- Table: MoyenPaiement
------------------------------------------------------------*/
CREATE TABLE MoyenPaiement(
	id                     INT IDENTITY (1,1) NOT NULL ,
	libelleMoyenPaiement   VARCHAR (25) NOT NULL  ,
	CONSTRAINT MoyenPaiement_PK PRIMARY KEY (id)
);


/*------------------------------------------------------------
-- Table: Reservation
------------------------------------------------------------*/
CREATE TABLE Reservation(
	id                 INT IDENTITY (1,1) NOT NULL ,
	dateDebut          DATETIME NOT NULL ,
	dateFin            DATETIME NOT NULL ,
	nbClient           INT  NOT NULL ,
	libelleFacture     VARCHAR (10)  ,
	id_Client          INT  NOT NULL ,
	id_MoyenPaiement   INT  NOT NULL  ,
	CONSTRAINT Reservation_PK PRIMARY KEY (id)

	,CONSTRAINT Reservation_Client_FK FOREIGN KEY (id_Client) REFERENCES Client(id)
	,CONSTRAINT Reservation_MoyenPaiement0_FK FOREIGN KEY (id_MoyenPaiement) REFERENCES MoyenPaiement(id)
);


/*------------------------------------------------------------
-- Table: LigneReservation
------------------------------------------------------------*/
CREATE TABLE LigneReservation(
	id                INT IDENTITY (1,1) NOT NULL ,
	coursInterieure   bit  NOT NULL ,
	balcon            bit  NOT NULL ,
	litBebe           bit  NOT NULL ,
	prixTotalLigne    FLOAT   ,
	numero            INT  NOT NULL ,
	id_Reservation    INT  NOT NULL  ,
	CONSTRAINT LigneReservation_PK PRIMARY KEY (id)

	,CONSTRAINT LigneReservation_Chambre_FK FOREIGN KEY (numero) REFERENCES Chambre(numero)
	,CONSTRAINT LigneReservation_Reservation0_FK FOREIGN KEY (id_Reservation) REFERENCES Reservation(id)
);


/*------------------------------------------------------------
-- Table: OptionServiceLigneReservation
------------------------------------------------------------*/
CREATE TABLE OptionServiceLigneReservation(
	id                    INT  NOT NULL ,
	id_LigneReservation   INT  NOT NULL ,
	date                  DATETIME NOT NULL  ,
	CONSTRAINT OptionServiceLigneReservation_PK PRIMARY KEY (id,id_LigneReservation)

	,CONSTRAINT OptionServiceLigneReservation_OptionService_FK FOREIGN KEY (id) REFERENCES OptionService(id)
	,CONSTRAINT OptionServiceLigneReservation_LigneReservation0_FK FOREIGN KEY (id_LigneReservation) REFERENCES LigneReservation(id)
);



